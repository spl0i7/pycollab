# Installation

### 1) Clone the repo and install dependencies
```bash
$ git clone https://gitlab.com/spl0i7/pycollab && cd pycollab
$ sudo pip3 install -r requirements.txt
```
### 2) Install Redis (Depending upon the operating system)
### 3) Launch the application 

```
$ python3 app.py
```

